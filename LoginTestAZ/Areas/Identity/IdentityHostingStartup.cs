﻿using System;
using LoginTestAZ.Areas.Identity.Data;
using LoginTestAZ.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(LoginTestAZ.Areas.Identity.IdentityHostingStartup))]
namespace LoginTestAZ.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<LoginTestAZContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("LoginTestAZContextConnection")));

                services.AddDefaultIdentity<AppUser>()
                    .AddEntityFrameworkStores<LoginTestAZContext>();
            });
        }
    }
}