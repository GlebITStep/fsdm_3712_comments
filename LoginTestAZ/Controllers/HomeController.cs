﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LoginTestAZ.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using LoginTestAZ.Areas.Identity.Data;
using Microsoft.EntityFrameworkCore;

namespace LoginTestAZ.Controllers
{
    public class HomeController : Controller
    {
        private readonly LoginTestAZContext context;
        private readonly UserManager<AppUser> userManager;

        public HomeController(LoginTestAZContext context, UserManager<AppUser> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Comments()
        {
            return View(context.Comments.Include(x => x.AppUser));
        }

        [HttpPost]
        public async Task<IActionResult> AddComment(Comment comment)
        {
            if (ModelState.IsValid && User.Identity.IsAuthenticated)
            {
                comment.Date = DateTime.Now;

                var user = await userManager.FindByNameAsync(User.Identity.Name);
                comment.AppUser = user;

                context.Comments.Add(comment);
                context.SaveChanges();
            }
            return RedirectToAction("Comments");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
