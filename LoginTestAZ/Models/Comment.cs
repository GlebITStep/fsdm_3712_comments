﻿using LoginTestAZ.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginTestAZ.Models
{
    public class Comment
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public DateTime Date { get; set; }

        public string AppUserId { get; set; }
        public AppUser AppUser { get; set; }
    }
}
